#!/bin/bash
git tag $1
git push --tags
mvn versions:update-parent -DparentVersion=$1
mvn versions:commit
mvn clean install
git add --all
git commit -m "version updated"
git push

